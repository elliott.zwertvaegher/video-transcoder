export const useJobsStore = defineStore("jobs", () => {
  const jobs = ref<
    {
      id: number;
      filePath: string;
      convertedFilePath?: string;
      outputFormat: string;
      status: "pending" | "done" | "cancelled";
      convertionTime?: number;
      estimatedConvertionTime?: number;
      logs?: string;
      createdAt?: string;
      updatedAt?: string;
    }[]
  >([]);
  // const count = ref(0);
  // const doubleCount = computed(() => count.value * 2);
  // function increment() {
  //   count.value++;
  // }

  async function getJobs() {
    const res = await fetch(`${import.meta.env.VITE_API_URL}/conversions`, {
      method: "GET",
    });
    const json = await res.json();
    console.log(json);
    jobs.value = json;
  }

  async function startJob(id: number) {
    const j = jobs.value.find((j) => j.id === id);
    if (j) j.estimatedConvertionTime = -1;
    return await fetch(
      `${import.meta.env.VITE_API_URL}/conversions/start/${id}`,
      {
        method: "GET",
      }
    );
  }

  async function cancelJob(id: number) {
    return await fetch(
      `${import.meta.env.VITE_API_URL}/conversions/cancel/${id}`,
      {
        method: "PUT",
      }
    );
  }

  async function deleteJob(id: number) {
    return await fetch(`${import.meta.env.VITE_API_URL}/conversions/${id}`, {
      method: "delete",
    });
  }

  return { jobs, getJobs, startJob, cancelJob, deleteJob };
});
[
  {
    id: 1,
    filePath:
      "files/1663294531230_Screen Recording 2022-08-18 at 1.21.02 PM.mov",
    convertedFilePath: "94857239.mov",
    outputFormat: "mov",
    status: "done",
    createdAt: "2022-09-16T02:15:31.000Z",
    updatedAt: "2022-09-16T02:21:50.000Z",
  },
];
