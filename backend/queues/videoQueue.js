const Queue = require("bull");
const timestring = require("timestring");
const videoQueue = new Queue("video transcoding");
const models = require("../models");
const { Op } = require("sequelize");
var ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const convertVideo = (path, format) => {
  const fileName = path.replace(/\.[^/.]+$/, "");
  const convertedFilePath = `${fileName}_${+new Date()}.${format}`;
  return new Promise((resolve, reject) => {
    let start;
    ffmpeg(`${__dirname}/../files/${path}`)
      .setFfmpegPath(process.env.FFMPEG_PATH)
      .setFfprobePath(process.env.FFPROBE_PATH)
      // .inputOptions("-stats -benchmark")
      .inputOptions(["-hwaccel cuda", "-stats", "-benchmark"])
      .toFormat(format)
      .on("start", (commandLine) => {
        console.log(`Spawned Ffmpeg with command: ${commandLine}`);
        start = new Date();
      })
      .on("error", (err, stdout, stderr) => {
        console.log(err, stdout, stderr);
        reject(err);
      })
      .on("end", (stdout, stderr) => {
        console.log(stdout, stderr);
        const logs = stdout + "\n" + stderr;
        var re = new RegExp("bench:[ \t]*utime=.*\n", "g");
        const result = re.exec(logs);
        const benchmarkLine = result?.[0];
        const time = benchmarkLine?.split("rtime=")?.[1]?.replace("\n", "");

        resolve({
          convertedFilePath,
          convertionTime: time ? timestring(time, "ms") : null,
          logs,
        });
      })
      .saveToFile(`${__dirname}/../files/${convertedFilePath}`);
  });
};
videoQueue.process(async (job) => {
  const { id, path, outputFormat } = job.data;
  try {
    const conversions = await models.VideoConversion.findAll({ where: { id } });
    // Needs to be better by comparing with file of similar size and format
    const lastConverstion = await models.VideoConversion.findOne({
      where: { status: "done", [Op.not]: { convertionTime: null } },
      order: [["createdAt", "DESC"]],
    });
    const conv = conversions[0];
    if (conv.status == "cancelled") {
      return Promise.resolve();
    }
    await models.VideoConversion.update(
      {
        estimatedConvertionTime: lastConverstion
          ? lastConverstion.convertionTime
          : -1,
      },
      {
        where: { id },
      }
    );
    const pathObj = await convertVideo(path, outputFormat);
    const convertedFilePath = pathObj.convertedFilePath;
    const conversion = await models.VideoConversion.update(
      {
        convertedFilePath,
        status: "done",
        convertionTime: pathObj.convertionTime,
        logs: pathObj.logs,
      },
      {
        where: { id },
      }
    );
    Promise.resolve(conversion);
  } catch (error) {
    Promise.reject(error);
  }
});
export { videoQueue };
