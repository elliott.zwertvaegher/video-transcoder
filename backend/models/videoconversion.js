"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class VideoConversion extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  VideoConversion.init(
    {
      filePath: DataTypes.STRING,
      convertedFilePath: DataTypes.STRING,
      outputFormat: DataTypes.STRING,
      status: {
        type: DataTypes.ENUM("pending", "done", "cancelled"),
        defaultValue: "pending",
      },
      convertionTime: DataTypes.INTEGER,
      logs: DataTypes.TEXT,
      estimatedConvertionTime: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "VideoConversion",
    }
  );
  return VideoConversion;
};
