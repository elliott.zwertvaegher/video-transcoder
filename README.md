# Video transcoder

Simple FFMPEG based video trasncoder project based on https://levelup.gitconnected.com/how-to-build-a-simple-video-converter-52eaeaf248f1


## Setup (for MacOS)

Both project requires node 16

### backend

1. download `ffmpeg` and `ffprobe` binaries from https://ffmpeg.org/download.html#build-mac
2. setup `FFMPEG_PATH` and `FFPROBE_PATH` in .env file
3. run `npm install`
4. run `docker-compose up`
5. run `npx sequelize-cli db:migrate` to setup up the database
6. run `npm run dev`


### frontend

1. run `npm install`
2. run `npm run dev`
